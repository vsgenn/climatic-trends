

Загрузка координат точек CRU и получение точек CRU для России. Если брать прям эти точки, то можно избежать сглаживани для получения температуры.
Путь к климатическим данным: http://data.ceda.ac.uk/badc/cru/data/cru_ts/cru_ts_4.01/data/tmp/


```{r}
ncname <- "E:/Work/climatic/cru4.01/cru_ts4.01.1951.1960.tmp.dat.nc.gz"
ncin <- nc_open(ncname)
lon <- ncvar_get(ncin,"lon")
lat <- ncvar_get(ncin,"lat")
# прямоугольная область россии
lon = lon[lon >= 60 & lon <= 170]
lat = lat[lat >= 40 & lat <= 80]
rus_rect = expand.grid(lon, lat)
rus_rect = cbind(rus_rect, rep(0, nrow(rus_rect)))
for (i in 1:nrow(rus_rect))
  rus_rect[i, 3] = map.where(database="world", rus_rect[i, 1], rus_rect[i, 2])
colnames(rus_rect) = c("lon", "lat", "country")
rus_cords = rus_rect[rus_rect$country == "Russia", ]
rus_cords = na.omit(rus_cords)
rus_cords = rus_cords[, 1:2]
```

Получение среднегодовой температуры
```{r}
rus_result = rus_cords[]
for (year in 1960:2016){
  resTmp = getCruDataForYear(rus_cords, year, "tmp")
  yearTmp = rowMeans(resTmp[4:ncol(resTmp)], na.rm = T)
  rus_result = cbind(rus_result, yearTmp)
}
colnames(rus_result) = c("lon", "lat", 1960:2016)
```


Рассчет коэффициентов тренда
```{r}
temperatures = rus_result[, 3:ncol(rus_result)]
year_to_sep = 1986
t1 = temperatures[, 1:(year_to_sep - 1960 )]
t2 = temperatures[, (year_to_sep - 1960 + 1):ncol(temperatures)]
coefs = rus_cords[]
coefs = cbind(coefs, rep(-500, nrow(coefs)))
coefs = cbind(coefs, rep(-500, nrow(coefs)))
colnames(coefs) = c("lon", "lat", "k1", "k2")
for (i in 1:nrow(coefs)){
  y1 = unlist(t1[i, ])
  x1 = 1:ncol(t1)
  k1 = coef(lm(y1 ~ x1))[2]
  coefs[i, 3] = k1
  y2 = unlist(t2[i, ])
  x2 = 1:ncol(t2)
  k2 = coef(lm(y2 ~ x2))[2]
  coefs[i, 4] = k2
}
```

```{r}
library(OpenStreetMap)
library(ggplot2)

plotMapWitTmp = function(req_coords, tem, filename = "", w = 30, h = 15, z = 4){
  res = data.frame(matrix(NA, nrow(req_coords), 3))
  colnames(res) = c("lon", "lat", "tem")
  res$lon = req_coords$lon
  res$lat = req_coords$lat
  res$tem = tem

  map <- openmap(c(lat = max(res$lat) + 5, lon = min(res$lon) - 5),
                 c(lat = min(res$lat) - 5, lon = max(res$lon) + 5),zoom = z, minNumTiles=40, type="stamen-terrain")
  res = na.omit(res)
  map <- openproj(map)
  p = autoplot(map) + geom_point(data = res, aes(x=lon,y=lat,color=tem)) + scale_color_gradientn(colours = rev(rainbow(3)), limits=c(-0.01, 0.15))
  if (filename != "")
    ggsave(filename, width = w, height = h, units = "cm")
  p
}

suppressWarnings(plotMapWitTmp(coefs[, 1:2], coefs[, 3], "here1.png"))

suppressWarnings(plotMapWitTmp(coefs[, 1:2], coefs[, 4], "here2.png"))

suppressWarnings(plotMapWitTmp(coefs[, 1:2], rep(NA, nrow(coefs)), "here3.png", 100, 50, 4))

```











